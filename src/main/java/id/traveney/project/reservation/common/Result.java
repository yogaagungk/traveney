package id.traveney.project.reservation.common;

import id.traveney.project.reservation.entity.*;
import lombok.Getter;

import java.io.Serializable;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Getter
public class Result implements Serializable {

    private Integer id;

    private String statusCode;

    private Location location;

    private Role role;

    private User user;

    private Tour tour;

    private TourItinerary tourItinerary;

    private TourGallery tourGallery;

    private Guide guide;

    private TourGuide tourGuide;

    private StatusTransaction statusTransaction;

    public Result(String statusCode, Location location) {
        this.statusCode = statusCode;
        this.location = location;
    }

    public Result(String statusCode) {
        this.statusCode = statusCode;
    }

    public Result(String statusCode, Role role) {
        this.role = role;
        this.statusCode = statusCode;
    }

    public Result(String statusCode, User user) {
        this.statusCode = statusCode;
        this.user = user;
    }

    public Result(String statusCode, Tour tour) {
        this.statusCode = statusCode;
        this.tour = tour;
    }

    public Result(String statusCode, TourItinerary tourItinerary) {
        this.statusCode = statusCode;
        this.tourItinerary = tourItinerary;
    }

    public Result(String statusCode, TourGallery tourGallery) {
        this.statusCode = statusCode;
        this.tourGallery = tourGallery;
    }

    public Result(String statusCode, Guide guide) {
        this.statusCode = statusCode;
        this.guide = guide;
    }

    public Result(String statusCode, TourGuide tourGuide) {
        this.statusCode = statusCode;
        this.tourGuide = tourGuide;
    }

    public Result(String statusCode, StatusTransaction statusTransaction) {
        this.statusCode = statusCode;
        this.statusTransaction = statusTransaction;
    }
}
