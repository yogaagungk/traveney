package id.traveney.project.reservation.common;

/**
 * Contributor yogaagung
 * Date 25/06/18
 */

public class Table {

    public static final String TABLE_LOCATION = "master_location";
    public static final String SYSTEM_ROLE = "system_role";
    public static final String SYSTEM_USER = "system_user";
    public static final String TABLE_TOUR = "master_tour";
    public static final String TABLE_TOUR_ITINERARY = "tour_itinerary";
    public static final String TABLE_TOUR_GALLERY = "tour_gallery";
    public static final String TABLE_TOUR_FILLED = "tour_quota";
    public static final String TABLE_GUIDE = "guide";
    public static final String TABLE_TOUR_GUIDE = "tour_guide";
    public static final String TABLE_STATUS_TRANSACTION = "status_transaction";
    public static final String TABLE_PARTICIPANT = "participant";
    public static final String TABLE_TRANSACTION = "transaction";
}
