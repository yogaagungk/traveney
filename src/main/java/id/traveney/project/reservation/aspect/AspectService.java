package id.traveney.project.reservation.aspect;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.*;
import id.traveney.project.reservation.exception.DefaultException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */
@Aspect
@Component
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AspectService {

    /**
     * Catch all transactional from service repository method in Application
     * and shared all exception if occurs in the method
     *
     * @param joinPoint joinPoint
     * @return Result
     * @throws Throwable
     */
    @Around("(execution(* id.traveney.project.reservation.service.*.save*(..)) " +
            "|| execution(* id.traveney.project.reservation.service.*.update*(..)))")
    public Result processSave(ProceedingJoinPoint joinPoint) throws Throwable {
        Application.getLogger(this).info("***AspectJ*** save is catch !! intercepted : " + joinPoint.getSignature());

        if (joinPoint.getArgs()[0] instanceof Location) {
            Location object = (Location) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof Role) {
            Role object = (Role) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof User) {
            User object = (User) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof Tour) {
            Tour object = (Tour) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof TourItinerary) {
            TourItinerary object = (TourItinerary) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof Guide) {
            Guide object = (Guide) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof TourGuide) {
            TourGuide object = (TourGuide) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof TourGallery) {
            TourGallery object = (TourGallery) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        } else if (joinPoint.getArgs()[0] instanceof StatusTransaction) {
            StatusTransaction object = (StatusTransaction) joinPoint.getArgs()[0];

            return new Result(validateSaveMethod(joinPoint), object);
        }

        return new Result(validateSaveMethod(joinPoint));  //some method have no register yet or some method no need register with specific treatment just around here
    }

    private String validateSaveMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            joinPoint.proceed();

            return StatusCode.SAVE_SUCCESS;
        } catch (DefaultException e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            Application.getLogger(this).error(e.getMessage());

            return StatusCode.DB_EXCEPTION;
        } catch (DataIntegrityViolationException dae) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            Application.getLogger(this).error(dae.getMessage());

            return StatusCode.SAVE_DATA_EXIST;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            e.printStackTrace();

            Application.getLogger(this).error(e.getMessage());

            return StatusCode.SYSTEM_EXCEPTION;
        }
    }

    @Around("execution(* id.traveney.project.reservation.service.*.delete*(..))")
    public Result processDelete(ProceedingJoinPoint joinPoint) throws Throwable {
        Application.getLogger(this).info("***AspectJ*** delete is catch !! intercepted : " + joinPoint.getSignature());

        return new Result(validateDeleteMethod(joinPoint));  //some method have no register yet or some method no need register with specific treatment just around here
    }

    /**
     * Catch all transactional for deleteImage from repository method in Application
     * and shared all exception if occurs in the method
     *
     * @param joinPoint jointPoint
     * @return Result
     * @throws Throwable
     */
    private String validateDeleteMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            joinPoint.proceed();

            return StatusCode.DELETE_SUCCESS;
        } catch (DataAccessException e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            Application.getLogger(this).error(e.getMessage());

            return StatusCode.DB_EXCEPTION;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            e.printStackTrace();

            Application.getLogger(this).error(e.getMessage());

            return StatusCode.SYSTEM_EXCEPTION;
        }
    }
}
