package id.traveney.project.reservation.service;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.TourGalleryDAO;
import id.traveney.project.reservation.entity.TourGallery;
import id.traveney.project.reservation.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class TourGalleryService extends AbstractGenericService<TourGallery> {

    @Value("${path.tour}")
    private String pathImage;

    @Autowired
    private TourGalleryDAO tourGalleryDAO;

    @Transactional
    public Result save(TourGallery entity, MultipartFile multipartFile) {
        Path path = Paths.get(pathImage);

        Application.getLogger(this).info(JsonUtil.toJson(entity));

        try {
            String completePath = path + "/" + UUID.randomUUID() + "-" + multipartFile.getOriginalFilename();

            File file = new File(completePath);
            file.setReadable(true);

            multipartFile.transferTo(file);

            entity.setTitle(file.getName());
            //entity.setUploadedBy(Application.getCurrentPrinciple());

            tourGalleryDAO.save(entity);

        } catch (Exception e) {
            Application.getLogger(this).error(e.getMessage());
        }

        return null;
    }
}
