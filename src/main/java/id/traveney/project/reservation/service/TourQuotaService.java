package id.traveney.project.reservation.service;

import id.traveney.project.reservation.entity.TourQuota;
import org.springframework.stereotype.Service;

@Service
public class TourQuotaService extends AbstractGenericService<TourQuota> {
}
