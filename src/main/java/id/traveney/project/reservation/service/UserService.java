package id.traveney.project.reservation.service;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.dao.UserDAO;
import id.traveney.project.reservation.entity.Role;
import id.traveney.project.reservation.entity.User;
import id.traveney.project.reservation.exception.DefaultException;
import id.traveney.project.reservation.redis.UserRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserService implements BaseService<User> {

    @Value("${path.user}")
    private String pathImage;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserRedis userRedis;

    @Override
    @Transactional
    public Result save(User entity) {
        entity.setPassword(BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt()));
        userDAO.save(entity);

        return null;
    }

    @Transactional
    public Result update(User entity) {
        User user = userDAO.findByUsername(entity.getUsername());
        user.setAddress(entity.getAddress());
        user.setBirthOfDate(entity.getBirthOfDate());
        user.setPhone(entity.getPhone());
        user.setEmail(entity.getEmail());
        user.setRole(entity.getRole());

        return null;
    }

    @Transactional
    public Result updatePassword(User entity) {
        User user = userDAO.findByUsername(entity.getUsername());
        String oldPassword = user.getPassword();

        if (oldPassword == null) {
            throw new DefaultException(StatusCode.PASSWORD_NOT_VALID);
        }

        if (BCrypt.checkpw(entity.getOldPassword(), oldPassword)) {
            user.setPassword(BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt()));

            entity.setPassword(null);
        } else {
            throw new DefaultException(StatusCode.PASSWORD_NOT_VALID);
        }

        return null;
    }

    @Transactional
    public Result uploadImage(User entity, MultipartFile multipartFile) {
        Path path = Paths.get(pathImage);

        try {
            String completePath = path + "/" + UUID.randomUUID() + "-" + multipartFile.getOriginalFilename();

            File file = new File(completePath);
            file.setReadable(true);

            Files.createDirectories(path);
            multipartFile.transferTo(file);

            entity.setImageProfile(file.getName());

            User user = userDAO.findByUsername(entity.getUsername());
            user.setImageProfile(file.getName());
        } catch (Exception e) {
            Application.getLogger(this).error(e.getMessage());
        }

        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> find(User param, int offset, int limit) {
        List<User> users = (List<User>) userDAO.find(param, offset, limit);

        for (User user : users) {
            user.setRole(new Role(user.getRole().getId()));
        }

        return users;
    }

    @Override
    public Long count(User param) {
        return null;
    }

    @Override
    @Transactional
    public Result delete(User entity) {
        User user = userDAO.findByUsername(entity.getUsername());
        user.setDeletedBy(Application.getCurrentPrinciple());
        user.setDeletedDate(new Date());

        return null;
    }

    @Override
    public User findById(Integer id) {
        return null;
    }

    @Transactional(readOnly = true)
    public User login(User entity) {
        User currentUser = userDAO.findByUsername(entity.getUsername());

        if (currentUser == null) {
            return null;
        }

        if (BCrypt.checkpw(entity.getPassword(), currentUser.getPassword())) {
            return userRedis.save(currentUser);
        }

        return null;
    }

    public void logout() {
        User user = Application.getCurrentPrinciple();

        if (user != null) {
            userRedis.delete(user.getRedisKey());
        }
    }
}