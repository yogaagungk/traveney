package id.traveney.project.reservation.service;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.TourDAO;
import id.traveney.project.reservation.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TourService implements BaseService<Tour> {

    @Autowired
    private TourDAO tourDAO;

    @Override
    @Transactional
    public Result save(Tour entity) {
        if (entity.getId() != null) {
            Tour tour = tourDAO.findReference(entity.getId());
            tour.setName(entity.getName());
            tour.setDescription(entity.getDescription());
            tour.setDateStart(entity.getDateStart());
            tour.setDateEnd(entity.getDateEnd());
            tour.setDeparture(entity.getDeparture());
            tour.setLocation(entity.getLocation());
            tour.setPrice(entity.getPrice());
        } else {
            entity.getTourQuota().setTour(entity);
            entity.setCreatedBy(Application.getCurrentPrinciple());

            tourDAO.save(entity);
        }

        return null;
    }

    @Transactional
    @Override
    public Result delete(Tour entity) {
        tourDAO.delete(tourDAO.findReference(entity.getId()));

        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Tour findById(Integer id) {
        Tour tour = tourDAO.findById(id);
        tour.setLocation(new Location(tour.getLocation().getName()));
        tour.setCreatedBy(new User(tour.getCreatedBy().getUsername()));

        return tour;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Tour> find(Tour param, int offset, int limit) {
        List<Tour> tours = (List<Tour>) tourDAO.find(param, offset, limit);

        for (Tour tour : tours) {
            tour.setLocation(new Location(tour.getLocation().getName()));
            tour.setCreatedBy(new User(tour.getCreatedBy().getUsername()));

            for (TourItinerary itinerary : tour.getTourItineraries()){
                itinerary.setDay(itinerary.getDay());
                itinerary.setDescription(itinerary.getDescription());
            }

            TourQuota tourQuota = new TourQuota();
            tourQuota.setFilledQuota(tour.getTourQuota().getFilledQuota());
            tourQuota.setMaxQuota(tour.getTourQuota().getMaxQuota());

            tour.setTourQuota(tourQuota);
        }

        return tours;
    }

    @Transactional(readOnly = true)
    @Override
    public Long count(Tour param) {
        return tourDAO.count(param);
    }
}
