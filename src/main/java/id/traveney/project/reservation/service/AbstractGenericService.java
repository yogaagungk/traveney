package id.traveney.project.reservation.service;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.entity.EntityAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

public class AbstractGenericService<T extends EntityAware> implements BaseService<T> {

    @Autowired
    private AbstractGenericDAO<T> dao;

    @Transactional
    @Override
    public Result save(T entity) {
        dao.save(entity);

        return null;
    }

    @Transactional
    @Override
    public Result delete(T entity) {
        dao.delete(entity);

        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public T findById(Integer id) {
        return dao.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<T> find(T param, int offset, int limit) {
        return dao.find(param, offset, limit);
    }

    @Transactional(readOnly = true)
    @Override
    public Long count(T param) {
        return dao.count(param);
    }

    @Transactional(readOnly = true)
    public T findReference(Integer id) {
        return dao.findReference(id);
    }
}
