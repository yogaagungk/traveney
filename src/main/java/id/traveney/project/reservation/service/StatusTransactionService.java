package id.traveney.project.reservation.service;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.StatusTransactionDAO;
import id.traveney.project.reservation.entity.StatusTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StatusTransactionService extends AbstractGenericService<StatusTransaction> {

    @Autowired
    private StatusTransactionDAO dao;

    @Transactional
    public Result save(StatusTransaction role) {
        dao.save(role);

        return null;
    }

    @Transactional(readOnly = true)
    public List<StatusTransaction> find() {
        return (List<StatusTransaction>) dao.find(null, 0, Integer.MAX_VALUE);
    }
}
