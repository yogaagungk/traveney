package id.traveney.project.reservation.service;

import id.traveney.project.reservation.entity.Guide;
import org.springframework.stereotype.Service;

@Service
public class GuideService extends AbstractGenericService<Guide> {
}
