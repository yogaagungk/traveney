package id.traveney.project.reservation.service;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.TourItineraryDAO;
import id.traveney.project.reservation.entity.TourItinerary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TourItineraryService extends AbstractGenericService<TourItinerary> {

    @Autowired
    private TourItineraryDAO tourItineraryDAO;

    @Override
    public Result save(TourItinerary entity) {
        if (entity.getId() != null) {
            TourItinerary tourItinerary = tourItineraryDAO.findReference(entity.getId());
            tourItinerary.setDay(entity.getDay());
            tourItinerary.setDescription(entity.getDescription());
        } else {
            entity.setCreatedBy(Application.getCurrentPrinciple());

            tourItineraryDAO.save(entity);
        }

        return null;
    }

    @Transactional
    public Result save(List<TourItinerary> tourItineraries) {
        tourItineraryDAO.saveBatch(tourItineraries);

        return null;
    }
}
