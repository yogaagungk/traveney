package id.traveney.project.reservation.service;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.LocationDAO;
import id.traveney.project.reservation.entity.Location;
import id.traveney.project.reservation.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Service
public class LocationService implements BaseService<Location> {

    private final LocationDAO locationDAO;

    @Autowired
    public LocationService(LocationDAO locationDAO) {
        this.locationDAO = locationDAO;
    }

    @Transactional
    @Override
    public Result save(Location entity) {
        if (entity.getId() == null) {
            entity.setCreatedBy(Application.getCurrentPrinciple());

            locationDAO.save(entity);
        } else {
            Location location = locationDAO.findReference(entity.getId());
            location.setName(entity.getName());
        }

        return null;
    }

    @Transactional
    @Override
    public Result delete(Location entity) {
        locationDAO.delete(locationDAO.findReference(entity.getId()));

        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<Location> find(Location param, int offset, int limit) {
        Collection<Location> locations = new ArrayList<>(locationDAO.find(param, offset, limit));

        for (Location location : locations) {
            location.setCreatedBy(new User(location.getCreatedBy().getUsername()));
        }

        return locations;
    }

    @Transactional(readOnly = true)
    @Override
    public Long count(Location param) {
        return locationDAO.count(param);
    }

    @Transactional(readOnly = true)
    @Override
    public Location findById(Integer id) {
        Location location = locationDAO.findReference(id);
        location.setCreatedBy(new User(location.getCreatedBy().getUsername()));

        return location;
    }

}
