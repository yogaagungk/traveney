package id.traveney.project.reservation.service;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.TransactionDAO;
import id.traveney.project.reservation.entity.Participant;
import id.traveney.project.reservation.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionService extends AbstractGenericService<Transaction> {

    @Autowired
    private TransactionDAO transactionDAO;

    @Transactional
    public Result save(Transaction param) {
        for (Participant participant : param.getParticipants()) {
            participant.setTransaction(param);
        }

        transactionDAO.save(param);

        return null;
    }
}
