package id.traveney.project.reservation.service;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.dao.RoleDAO;
import id.traveney.project.reservation.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Service
public class RoleService {

    @Autowired
    private RoleDAO dao;

    @Transactional
    public Result save(Role role) {
        dao.save(role);

        return null;
    }

    @Transactional(readOnly = true)
    public List<Role> find() {
        return (List<Role>) dao.find(null, 0, Integer.MAX_VALUE);
    }

    @Transactional(readOnly = true)
    public Role find(Role.RoleName param) {
        return dao.find(param);
    }
}
