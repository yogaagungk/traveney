package id.traveney.project.reservation.service;

import id.traveney.project.reservation.common.Result;

import java.util.Collection;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

public interface BaseService<T> {

    Result save(final T entity);

    Result delete(final T entity);

    T findById(final Integer id);

    Collection<T> find(T param, int offset, int limit);

    Long count(T param);
}
