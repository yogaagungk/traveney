package id.traveney.project.reservation.service;

import id.traveney.project.reservation.entity.TourGuide;
import org.springframework.stereotype.Service;

@Service
public class TourGuideService extends AbstractGenericService<TourGuide> {
}
