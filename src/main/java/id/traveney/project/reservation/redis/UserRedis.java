package id.traveney.project.reservation.redis;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.authentication.JwtTokenService;
import id.traveney.project.reservation.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserRedis {

    @Autowired
    private JwtTokenService tokenService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     *
     * @param param
     * @return
     */
    public User save(User param){
        User user = new User();
        user.setId(param.getId());
        user.setUsername(param.getUsername());
        user.setEmail(param.getUsername());
        user.setRole(param.getRole());
        user.setToken(tokenService.generateToken(param));
        user.setRedisKey(param.getRedisKey());

        redisTemplate.opsForValue().set(user.getRedisKey(), user);
        redisTemplate.expireAt(user.getRedisKey(), Application.getExpiredToken());

        return user;
    }

    public User get(String key){
        return (User) redisTemplate.opsForValue().get(key);
    }

    public boolean delete(String key){
        return redisTemplate.delete(key);
    }

}
