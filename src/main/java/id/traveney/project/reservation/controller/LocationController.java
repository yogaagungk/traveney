package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Location;
import id.traveney.project.reservation.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@RestController
public class LocationController extends BaseController {

    @Autowired
    private LocationService service;

    @GetMapping(value = "locations")
    public Map<String, Object> find(@RequestParam(name = "id", required = false) Integer id) {
        if (id != null) {
            Location location = service.findById(id);

            if (location != null) {
                return convertModel(location);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        Location param = new Location();

        return convertModel(
                new ArrayList<>(service.find(param, 0, 100)),
                service.count(param)
        );
    }

    @PostMapping(value = "locations")
    public Map<String, Object> save(@RequestBody Location entity) {
        Result result = service.save(entity);

        return convertModel(result.getLocation(), result.getStatusCode());
    }

    @PutMapping(value = "locations")
    public Map<String, Object> update(@RequestBody Location entity) {
        Result result = service.save(entity);

        return convertModel(result.getLocation(), result.getStatusCode());
    }

    @DeleteMapping(value = "locations/{id}")
    public Map<String, Object> delete(@PathVariable(value = "id") Integer id) {
        return convertModel(service.delete(new Location(id)).getStatusCode());
    }
}
