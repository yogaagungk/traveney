package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Location;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TourController extends BaseController {

    @Autowired
    private TourService service;

    @GetMapping(value = "tours")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "name", required = false) String name,
                                    @RequestParam(name = "location", required = false) Integer locationId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            Tour tour = service.findById(id);

            if (tour != null) {
                return convertModel(tour);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        Tour param = new Tour();

        if (name != null) {
            param.setName(name);
        }

        if (locationId != null) {
            param.setLocation(new Location(locationId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "tours")
    public Map<String, Object> save(@RequestBody Tour tour) {

        return convertModel(service.save(tour).getStatusCode());
    }

    @PutMapping(value = "tours")
    public Map<String, Object> update(@RequestBody Tour tour) {

        return convertModel(service.save(tour).getStatusCode());
    }

    @DeleteMapping(value = "tours/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(new Tour(id)).getStatusCode());
    }
}
