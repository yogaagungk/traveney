package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.entity.TourQuota;
import id.traveney.project.reservation.service.TourQuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TourQuotaController extends BaseController {

    @Autowired
    private TourQuotaService service;

    @GetMapping(value = "tours-quota")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "tour", required = false) Integer tourId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            TourQuota tourQuota = service.findById(id);

            if (tourQuota != null) {
                return convertModel(tourQuota);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        TourQuota param = new TourQuota();

        if (tourId != null) {
            param.setTour(new Tour(tourId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "tours-quota")
    public Map<String, Object> save(@RequestBody TourQuota tourQuota) {

        return convertModel(service.save(tourQuota).getStatusCode());
    }

    @PutMapping(value = "tours-quota")
    public Map<String, Object> update(@RequestBody TourQuota tourQuota) {

        return convertModel(service.save(tourQuota).getStatusCode());
    }

    @DeleteMapping(value = "tours-quota/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findReference(id)).getStatusCode());
    }
}
