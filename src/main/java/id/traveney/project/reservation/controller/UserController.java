package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.User;
import id.traveney.project.reservation.service.UserService;
import id.traveney.project.reservation.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class UserController extends BaseController {

    @Autowired
    private UserService service;

    @PreAuthorize("permitAll()")
    @PostMapping("do-login")
    public Map<String, Object> login(@RequestBody User entity) {
        User user = service.login(entity);

        if (user != null) {
            return convertModel(user.getToken(), StatusCode.LOGIN_SUCCESS);
        }

        return convertModel(StatusCode.LOGIN_FAILED);
    }

    @DeleteMapping(value = "do-logout")
    public Map<String, Object> logout() {
        service.logout();

        return convertModel(StatusCode.OPERATION_COMPLETE);
    }

    @GetMapping(value = "users")
    public Map<String, Object> find() {

        return convertModel(service.find(new User(), 0, 10));
    }

    @PostMapping(value = "users")
    public Map<String, Object> save(@RequestBody User entity) {

        return convertModel(service.save(entity).getStatusCode());
    }

    @PutMapping(value = "users")
    public Map<String, Object> update(@RequestBody User entity) {

        return convertModel(service.update(entity).getStatusCode());
    }

    @PutMapping(value = "users/update-password")
    public Map<String, Object> updatePassword(@RequestBody User entity) {

        return convertModel(service.updatePassword(entity).getStatusCode());
    }

    @PutMapping(value = "users/upload-image")
    public Map<String, Object> uploadImage(@RequestPart(name = "file") MultipartFile file,
                                           @RequestParam(name = "user") String user) {

        Result result = service.uploadImage(JsonUtil.fromJson(user, User.class), file);

        return convertModel(result.getStatusCode());
    }

}
