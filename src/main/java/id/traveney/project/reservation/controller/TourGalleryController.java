package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.Result;
import id.traveney.project.reservation.entity.TourGallery;
import id.traveney.project.reservation.service.TourGalleryService;
import id.traveney.project.reservation.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class TourGalleryController extends BaseController {

    @Autowired
    private TourGalleryService service;

    @PostMapping(value = "tour-galleries")
    public Map<String, Object> save(@RequestPart(name = "files") MultipartFile multipartFile,
                                    @RequestParam(name= "tourGallery") String tourGallery) {

        Result result = service.save(JsonUtil.fromJson(tourGallery, TourGallery.class), multipartFile);

        return convertModel(result);
    }

    @DeleteMapping(value = "tour-galleries/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findReference(id)).getStatusCode());
    }

}
