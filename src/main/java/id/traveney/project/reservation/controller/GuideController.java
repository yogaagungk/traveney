package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Guide;
import id.traveney.project.reservation.entity.Location;
import id.traveney.project.reservation.service.GuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class GuideController extends BaseController {

    @Autowired
    private GuideService service;

    @GetMapping(value = "guides")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "location", required = false) Integer locationId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            Guide guide = service.findById(id);

            if (guide != null) {
                return convertModel(guide);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        Guide param = new Guide();

        if (locationId != null) {
            param.setLocation(new Location(locationId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "guides")
    public Map<String, Object> save(@RequestBody Guide guide) {

        return convertModel(service.save(guide).getStatusCode());
    }

    @PutMapping(value = "guides")
    public Map<String, Object> update(@RequestBody Guide guide) {

        return convertModel(service.save(guide).getStatusCode());
    }

    @DeleteMapping(value = "guides/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findReference(id)).getStatusCode());
    }
}
