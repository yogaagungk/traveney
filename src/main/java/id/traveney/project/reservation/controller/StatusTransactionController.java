package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.service.StatusTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class StatusTransactionController extends BaseController {

    @Autowired
    private StatusTransactionService service;

    @GetMapping(value = "status-transaction")
    public Map<String, Object> find() {
        return convertModel(service.find());
    }
}
