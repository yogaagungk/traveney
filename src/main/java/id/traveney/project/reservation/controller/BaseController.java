package id.traveney.project.reservation.controller;

import org.springframework.security.access.prepost.PreAuthorize;

import java.util.HashMap;
import java.util.Map;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@PreAuthorize("isFullyAuthenticated()")
public class BaseController {

    public Map<String, Object> convertModel(Object data, Object status) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("status", status);

        return model;
    }

    public Map<String, Object> convertModel(Object data) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);

        return model;
    }

    public Map<String, Object> convertModel(String status) {
        Map<String, Object> model = new HashMap<>();
        model.put("status", status);

        return model;
    }

    public Map<String, Object> convertModel(Object data, Long row) {
        Map<String, Object> model = new HashMap<>();
        model.put("data", data);
        model.put("row", row);

        return model;
    }
}
