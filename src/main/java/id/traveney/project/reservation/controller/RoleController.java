package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RoleController extends BaseController {

    @Autowired
    private RoleService service;

    @GetMapping(value = "roles")
    public Map<String, Object> find() {
        return convertModel(service.find());
    }
}
