package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.entity.TourItinerary;
import id.traveney.project.reservation.service.TourItineraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TourItineraryController extends BaseController {

    @Autowired
    private TourItineraryService service;

    @GetMapping(value = "tour-itineraries")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "tour", required = false) Integer tourId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            TourItinerary tourItinerary = service.findById(id);

            if (tourItinerary != null) {
                return convertModel(tourItinerary);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        TourItinerary param = new TourItinerary();

        if (tourId != null) {
            param.setTour(new Tour(tourId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "tour-itineraries")
    public Map<String, Object> save(@RequestBody List<TourItinerary> tourItineraries) {

        return convertModel(service.save(tourItineraries).getStatusCode());
    }

    @PutMapping(value = "tour-itineraries")
    public Map<String, Object> update(@RequestBody List<TourItinerary> tourItineraries) {

        return convertModel(service.save(tourItineraries).getStatusCode());
    }

    @DeleteMapping(value = "tour-itineraries/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findReference(id)).getStatusCode());
    }
}
