package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.entity.Transaction;
import id.traveney.project.reservation.entity.User;
import id.traveney.project.reservation.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TransactionController extends BaseController {

    @Autowired
    private TransactionService service;

    @GetMapping(value = "transactions")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "tour", required = false) Integer tourId,
                                    @RequestParam(name = "user", required = false) Integer userId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            Transaction transaction = service.findById(id);

            if (transaction != null) {
                return convertModel(transaction);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        Transaction param = new Transaction();

        if (tourId != null) {
            param.setTour(new Tour(tourId));
        }

        if (userId != null) {
            param.setUser(new User(userId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "transactions")
    public Map<String, Object> save(@RequestBody Transaction transaction) {

        return convertModel(service.save(transaction).getStatusCode());
    }

    @PutMapping(value = "transactions")
    public Map<String, Object> update(@RequestBody Transaction transaction) {

        return convertModel(service.save(transaction).getStatusCode());
    }

    @DeleteMapping(value = "transactions/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findById(id)));
    }
}