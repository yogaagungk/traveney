package id.traveney.project.reservation.controller;

import id.traveney.project.reservation.common.StatusCode;
import id.traveney.project.reservation.entity.Guide;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.entity.TourGuide;
import id.traveney.project.reservation.service.TourGuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TourGuideController extends BaseController {

    @Autowired
    private TourGuideService service;

    @GetMapping(value = "tour-guides")
    public Map<String, Object> find(@RequestParam(name = "offset") int offset,
                                    @RequestParam(name = "limit") int limit,
                                    @RequestParam(name = "id", required = false) Integer id,
                                    @RequestParam(name = "tour", required = false) Integer tourId,
                                    @RequestParam(name = "guide", required = false) Integer guideId) {

        if (limit > 100) {
            return convertModel(StatusCode.DATA_EXCEED_LIMIT);
        }

        if (id != null) {
            TourGuide tourGuide = service.findById(id);

            if (tourGuide != null) {
                return convertModel(tourGuide);
            }

            return convertModel(StatusCode.DATA_NOT_FOUND);
        }

        TourGuide param = new TourGuide();

        if (tourId != null) {
            param.setTour(new Tour(tourId));
        }

        if (guideId != null) {
            param.setGuide(new Guide(guideId));
        }

        return convertModel(service.find(param, offset, limit), service.count(param));
    }

    @PostMapping(value = "tour-guides")
    public Map<String, Object> save(@RequestBody TourGuide tourGuide) {

        return convertModel(service.save(tourGuide).getStatusCode());
    }

    @PutMapping(value = "tour-guides")
    public Map<String, Object> update(@RequestBody TourGuide tourGuide) {

        return convertModel(service.save(tourGuide).getStatusCode());
    }

    @DeleteMapping(value = "tour-guides/{id}")
    public Map<String, Object> delete(@PathVariable(name = "id") Integer id) {

        return convertModel(service.delete(service.findReference(id)).getStatusCode());
    }
}
