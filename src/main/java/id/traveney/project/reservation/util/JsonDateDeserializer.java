package id.traveney.project.reservation.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contributor krissadewo
 * Date 01/06/18
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String date = jsonParser.getText().trim();
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
            try {
                return formatter.parse(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }
}
