package id.traveney.project.reservation.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contributor krissadewo
 * Date 01/06/18
 */
public class JsonDateSerializer extends StdSerializer<Date> {

    static final SimpleDateFormat JSON_SERIALIZE_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

    public JsonDateSerializer() {
        this(null);
    }

    public JsonDateSerializer(Class<Date> t) {
        super(t);
    }

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (value == null) {
            gen.writeNull();
        } else {
            gen.writeString(JSON_SERIALIZE_DATE_FORMAT.format(value.getTime()));
        }
    }
}

