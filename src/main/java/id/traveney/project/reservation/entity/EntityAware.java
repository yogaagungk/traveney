package id.traveney.project.reservation.entity;

import java.io.Serializable;

/**
 * Contributor yogaagung
 * Date 25/06/18
 */

public interface EntityAware extends Serializable {
}
