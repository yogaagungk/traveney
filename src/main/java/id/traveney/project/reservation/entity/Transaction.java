package id.traveney.project.reservation.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.traveney.project.reservation.util.JsonDateDeserializer;
import id.traveney.project.reservation.util.JsonDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.TABLE_TRANSACTION)
public class Transaction implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tour_id")
    private Tour tour;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "total_participant")
    private Integer totalParticipant;

    @OneToMany(mappedBy = "transaction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Participant> participants;

    @OneToOne(fetch = FetchType.LAZY)
    private StatusTransaction status;

    @Column(name = "payment")
    private String payment;

    @Column(name = "total_price")
    private Integer totalPrice;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private User createdBy;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIME)
    private Date createdDate = new Date();

    public void addParticipant(Participant participant) {
        participants.add(participant);
        participant.setTransaction(this);
    }
}
