package id.traveney.project.reservation.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.traveney.project.reservation.util.JsonDateDeserializer;
import id.traveney.project.reservation.util.JsonDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.TABLE_TOUR_FILLED)
public class TourQuota implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tour_id")
    private Tour tour;

    @Column(name = "max_quota")
    private Integer maxQuota;

    @Column(name = "filled_quota")
    private Integer filledQuota;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uploaded_by")
    private User uploadedBy;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "uploaded_date")
    @Temporal(TemporalType.TIME)
    private Date uploadedDate = new Date();
}
