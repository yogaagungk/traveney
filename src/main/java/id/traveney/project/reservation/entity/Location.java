package id.traveney.project.reservation.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.traveney.project.reservation.util.JsonDateDeserializer;
import id.traveney.project.reservation.util.JsonDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Contributor yogaagung
 * Date 25/06/18
 */

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.TABLE_LOCATION)
public class Location implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private User createdBy;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();

    public Location(Integer locationId) {
        this.id = locationId;
    }

    public Location(String name) {
        this.name = name;
    }
}
