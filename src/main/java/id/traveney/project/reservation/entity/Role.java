package id.traveney.project.reservation.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.SYSTEM_ROLE)
public class Role implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", unique = true, nullable = false)
    private RoleName name;

    @Getter
    public enum RoleName {
        ROLE_ADMIN,
        ROLE_USER,
        ROLE_CONTRIBUTOR,
        ROLE_GUIDE
    }

    public Role(RoleName roleName) {
        this.name = roleName;
    }

    public Role(Integer id) {
        this.id = id;
    }
}
