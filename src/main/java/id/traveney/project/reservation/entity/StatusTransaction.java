package id.traveney.project.reservation.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.TABLE_STATUS_TRANSACTION)
public class StatusTransaction implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", unique = true, nullable = false)
    private Status name;

    @Getter
    public enum Status {
        PENDING_PAYMENT,
        PAID,
        DONE
    }

    public StatusTransaction(Status statusName) {
        this.name = statusName;
    }

    public StatusTransaction(Integer id) {
        this.id = id;
    }
}
