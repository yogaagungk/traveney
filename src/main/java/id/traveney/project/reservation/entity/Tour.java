package id.traveney.project.reservation.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.traveney.project.reservation.util.JsonDateDeserializer;
import id.traveney.project.reservation.util.JsonDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = id.traveney.project.reservation.common.Table.TABLE_TOUR)
public class Tour implements EntityAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private Location location;

    @Column(name = "description")
    private String description;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "date_start")
    @Temporal(TemporalType.TIME)
    private Date dateStart;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "date_end")
    @Temporal(TemporalType.TIME)
    private Date dateEnd;

    @Column(name = "departure")
    private String departure;

    @Column(name = "price")
    private String price;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "tour")
    private List<TourItinerary> tourItineraries;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "tour", cascade = CascadeType.ALL)
    private TourQuota tourQuota;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private User createdBy;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIME)
    private Date createdDate = new Date();

    public Tour(Integer id) {
        this.id = id;
    }

    public void setItinerary(TourItinerary itinerary) {
        tourItineraries.add(itinerary);
        itinerary.setTour(this);
    }

    public void setQuota(TourQuota tourQuota) {
        this.tourQuota = tourQuota;
        tourQuota.setTour(this);
    }
}
