package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.LocationDAO;
import id.traveney.project.reservation.entity.Location;
import org.springframework.stereotype.Repository;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Repository
public class LocationDAOImpl extends AbstractGenericDAO<Location> implements LocationDAO {
}
