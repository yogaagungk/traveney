package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.User;

public interface UserDAO extends BaseDAO<User> {

    User findByUsername(String username);
}
