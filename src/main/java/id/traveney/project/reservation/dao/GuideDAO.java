package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.Guide;

public interface GuideDAO extends BaseDAO<Guide> {
}
