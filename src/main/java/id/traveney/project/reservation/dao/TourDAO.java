package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.Tour;

public interface TourDAO extends BaseDAO<Tour> {
}
