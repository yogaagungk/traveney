package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.TourQuota;

public interface TourQuotaDAO extends BaseDAO<TourQuota> {
}
