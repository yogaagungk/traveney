package id.traveney.project.reservation.dao;

import java.util.Collection;

/**
 * Contributor yogaagung
 * Date 25/06/18
 */

public interface BaseDAO<T> {

    T save(final T entity);

    T update(final T entity);

    T delete(final T entity);

    Collection<T> find(final T param, int offset, int limit);

    T findById(final Integer id);

    T findReference(Integer id);

    Long count(T param);
}
