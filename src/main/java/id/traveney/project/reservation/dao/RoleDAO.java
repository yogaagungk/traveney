package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.Role;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

public interface RoleDAO extends BaseDAO<Role> {

    Role find(Role.RoleName roleName);

}
