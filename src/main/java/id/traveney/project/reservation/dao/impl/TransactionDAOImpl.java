package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TransactionDAO;
import id.traveney.project.reservation.entity.Tour;
import id.traveney.project.reservation.entity.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class TransactionDAOImpl extends AbstractGenericDAO<Transaction> implements TransactionDAO {

    @Override
    public Collection<Transaction> find(Transaction param, int offset, int limit) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery query = builder.createQuery(Transaction.class);

        Root root = query.from(Transaction.class);

        List<Predicate> predicates = new ArrayList<>();

        if (param.getTour() != null) {
            Join<Transaction, Tour> tour = root.join("tour", JoinType.LEFT);

            predicates.add(builder.and(builder.equal(tour.get("id"), param.getTour().getId())));
        }

        query.distinct(true);
        query.where(predicates.toArray(new Predicate[0]));
        query.orderBy(builder.asc(root.get("tour")));

        return entityManager
                .createQuery(query)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public Long count(Transaction param) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery query = builder.createQuery(Long.class);

        Root root = query.from(Transaction.class);

        query.select(builder.count(root));

        List<Predicate> predicates = new ArrayList<>();

        if (param.getTour() != null) {
            Join<Transaction, Tour> tour = root.join("tour", JoinType.LEFT);

            predicates.add(builder.and(builder.equal(tour.get("id"), param.getTour().getId())));
        }

        query.where(predicates.toArray(new Predicate[0]));

        return (Long) entityManager.createQuery(query).getSingleResult();
    }
}
