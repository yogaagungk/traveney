package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TourItineraryDAO;
import id.traveney.project.reservation.entity.TourItinerary;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class TourItineraryDAOImpl extends AbstractGenericDAO<TourItinerary> implements TourItineraryDAO {

    @Override
    public Collection<TourItinerary> find(TourItinerary param, int offset, int limit) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<TourItinerary> query = builder.createQuery(TourItinerary.class);

        Root<TourItinerary> root = query.from(TourItinerary.class);

        List<Predicate> predicates = new ArrayList<>();

        if (param.getTour() != null) {
            Join tour = root.join("tour", JoinType.LEFT);

            predicates.add(builder.and(builder.equal(tour.get("id"), param.getTour().getId())));
        }

        query.distinct(true);
        query.where(predicates.toArray(new Predicate[0]));
        query.orderBy(builder.asc(root.get("tour")));

        return entityManager
                .createQuery(query)
                .getResultList();

    }

    @Override
    public List<TourItinerary> saveBatch(List<TourItinerary> entities) {
        List<TourItinerary> savedEntities = new ArrayList<>();
        int i = 0;

        for (TourItinerary entity : entities) {
            savedEntities.add(persistOrMerge(entity));

            i++;

            if (i % 100 == 0) {
                entityManager.flush();
                entityManager.clear();
            }
        }

        return savedEntities;
    }

    private TourItinerary persistOrMerge(TourItinerary periode) {
        if (periode.getId() == null) {
            entityManager.persist(periode);

            return periode;
        } else {
            return entityManager.merge(periode);
        }
    }
}
