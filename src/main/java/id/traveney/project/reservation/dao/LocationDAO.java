package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.Location;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

public interface LocationDAO extends BaseDAO<Location> {
}
