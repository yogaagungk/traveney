package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.TourGallery;

public interface TourGalleryDAO extends BaseDAO<TourGallery> {
}
