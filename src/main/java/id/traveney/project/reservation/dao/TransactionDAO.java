package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.Transaction;

import java.util.List;

public interface TransactionDAO extends BaseDAO<Transaction> {

}
