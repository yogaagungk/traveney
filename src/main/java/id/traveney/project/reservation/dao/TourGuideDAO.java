package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.TourGuide;

public interface TourGuideDAO extends BaseDAO<TourGuide> {
}
