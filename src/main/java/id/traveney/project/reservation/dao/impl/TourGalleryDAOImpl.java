package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TourGalleryDAO;
import id.traveney.project.reservation.entity.TourGallery;
import org.springframework.stereotype.Repository;

@Repository
public class TourGalleryDAOImpl extends AbstractGenericDAO<TourGallery> implements TourGalleryDAO {
}
