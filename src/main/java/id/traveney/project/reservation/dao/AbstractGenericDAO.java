package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.EntityAware;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

/**
 * Contributor yogaagung
 * Date 25/06/18
 */

public class AbstractGenericDAO<T extends EntityAware> implements BaseDAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    private Class<T> type;

    {
        this.type= (Class<T>)
                ((ParameterizedType) getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Override
    public T save(T entity) {
        entityManager.persist(entity);

        return entity;
    }

    @Override
    public T update(T entity) {
        entityManager.merge(entity);

        return entity;
    }

    @Override
    public T delete(T entity) {
        entityManager.remove(entity);

        return entity;
    }


    @Override
    public Collection<T> find(T param, int offset, int limit) {
        CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(type);
        query.select(query.from(type));

        return entityManager.createQuery(query)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public T findById(Integer id) {
        return entityManager.find(type, id);
    }

    @Override
    public T findReference(Integer id) {
        return (T) Hibernate.unproxy(entityManager.getReference(type, id));
    }

    @Override
    public Long count(T param) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<T> root = query.from(type);
        query.select(builder.count(root));

        return entityManager.createQuery(query).getSingleResult();
    }
}
