package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TourGuideDAO;
import id.traveney.project.reservation.entity.TourGuide;
import org.springframework.stereotype.Repository;

@Repository
public class TourGuideDAOImpl extends AbstractGenericDAO<TourGuide> implements TourGuideDAO {
}
