package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.RoleDAO;
import id.traveney.project.reservation.entity.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

@Repository
public class RoleDAOImpl extends AbstractGenericDAO<Role> implements RoleDAO {

    @Override
    public Role find(Role.RoleName roleName) {
        String sql = "SELECT role FROM Role role " +
                "WHERE role.name = :name ";

        try {
            return (Role) entityManager.createQuery(sql)
                    .setParameter("name", roleName)
                    .getSingleResult();
        }catch (NoResultException e){
        }

        return null;
    }
}
