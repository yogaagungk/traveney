package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TourQuotaDAO;
import id.traveney.project.reservation.entity.TourQuota;
import org.springframework.stereotype.Repository;

@Repository
public class TourQuotaDAOImpl extends AbstractGenericDAO<TourQuota> implements TourQuotaDAO {
}
