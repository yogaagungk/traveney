package id.traveney.project.reservation.dao;

import id.traveney.project.reservation.entity.TourItinerary;

import java.util.List;

public interface TourItineraryDAO extends BaseDAO<TourItinerary> {

    List<TourItinerary> saveBatch(List<TourItinerary> tourItineraries);
}
