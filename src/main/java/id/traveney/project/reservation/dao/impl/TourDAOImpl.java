package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.TourDAO;
import id.traveney.project.reservation.entity.Location;
import id.traveney.project.reservation.entity.Tour;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class TourDAOImpl extends AbstractGenericDAO<Tour> implements TourDAO {

    @Override
    public Collection<Tour> find(Tour param, int offset, int limit) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tour> query = builder.createQuery(Tour.class);
        Root<Tour> root = query.from(Tour.class);

        List<Predicate> predicates = new ArrayList<>();

        if (param.getName() != null) {
            predicates.add(builder.and(builder.like(root.get("name"), param.getName())));
        }

        if (param.getLocation() != null) {
            Join<Tour, Location> location = root.join("location", JoinType.LEFT);
            predicates.add(builder.and(builder.equal(location.get("id"), param.getLocation().getId())));
        }

        query.distinct(true);
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        query.orderBy(builder.asc(root.get("id")));

        return entityManager
                .createQuery(query)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public Long count(Tour param) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<Tour> root = query.from(Tour.class);
        query.select(builder.count(root));

        List<Predicate> predicates = new ArrayList<>();

        if (param.getName() != null) {
            predicates.add(builder.and(builder.like(root.get("name"), param.getName())));
        }

        if (param.getLocation() != null) {
            Join<Tour, Location> location = root.join("location", JoinType.LEFT);
            predicates.add(builder.and(builder.equal(location.get("id"), param.getLocation().getId())));
        }

        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return entityManager
                .createQuery(query)
                .getSingleResult();
    }
}
