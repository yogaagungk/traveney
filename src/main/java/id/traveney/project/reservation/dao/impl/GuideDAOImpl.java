package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.GuideDAO;
import id.traveney.project.reservation.entity.Guide;
import org.springframework.stereotype.Repository;

@Repository
public class GuideDAOImpl extends AbstractGenericDAO<Guide> implements GuideDAO {
}
