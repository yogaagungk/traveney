package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.UserDAO;
import id.traveney.project.reservation.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

@Repository
public class UserDAOImpl extends AbstractGenericDAO<User> implements UserDAO {

    @Override
    public User findByUsername(String username) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);

        Root<User> root = query.from(User.class);
        root.fetch("role", JoinType.INNER);

        try{
            return entityManager.createQuery(query).getSingleResult();
        }catch (NoResultException e){
            Application.getLogger(this).error(e.getMessage());
        }

        return null;
    }
}
