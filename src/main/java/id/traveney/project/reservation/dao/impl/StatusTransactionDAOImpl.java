package id.traveney.project.reservation.dao.impl;

import id.traveney.project.reservation.dao.AbstractGenericDAO;
import id.traveney.project.reservation.dao.StatusTransactionDAO;
import id.traveney.project.reservation.entity.StatusTransaction;
import org.springframework.stereotype.Repository;

@Repository
public class StatusTransactionDAOImpl extends AbstractGenericDAO<StatusTransaction> implements StatusTransactionDAO {
}
