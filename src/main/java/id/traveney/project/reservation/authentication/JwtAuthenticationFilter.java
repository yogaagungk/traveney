package id.traveney.project.reservation.authentication;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.entity.User;
import id.traveney.project.reservation.redis.UserRedis;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    public static final String tokenHeader = "Authorization";

    private JwtTokenService tokenService;

    private UserRedis userRedis;

    public JwtAuthenticationFilter(JwtTokenService jwtTokenService, UserRedis userRedis) {
        this.tokenService = jwtTokenService;
        this.userRedis = userRedis;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authToken = request.getHeader(tokenHeader);

        if (authToken != null) {
            String key = tokenService.getRedisKeyFromToken(authToken);

            Application.getLogger(this).info("checking authentication for user " + key);

            if (key != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                if (tokenService.validateToken(authToken)) {
                    User user = userRedis.get(key);
                    user.setToken(authToken);

                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
                            null, user.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authentication);

                    Application.getLogger(this).info("authenticated user " + key + ", setting security context");

                    response.setStatus(HttpServletResponse.SC_OK);
                } else {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }

        filterChain.doFilter(request, response);
    }
}
