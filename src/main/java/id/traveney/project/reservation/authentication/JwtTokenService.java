package id.traveney.project.reservation.authentication;

import id.traveney.project.reservation.Application;
import id.traveney.project.reservation.entity.User;
import id.traveney.project.reservation.redis.UserRedis;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenService implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;

    private static final String CLAIM_USERNAME = "sub";
    private static final String CLAIM_AUDIENCE = "audience";
    private static final String CLAIM_CREATED = "created";
    private static final String CLAIM_EXPIRATION = "exp";
    private static final String CLAIM_ROLE = "role";
    private static final String AUDIENCE_WEB = "WEB";
    private static final String AUDIENCE_MOBILE = "MOBILE";

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRedis userRedis;

    public String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Application.getExpiredToken())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_USERNAME, user.getUsername());
        claims.put(CLAIM_AUDIENCE, generateAudience(user));
        claims.put(CLAIM_ROLE, user.getRole());
        claims.put(CLAIM_CREATED, new Date());

        return generateToken(claims);
    }

    public String generateAudience(User user) {
        return AUDIENCE_WEB;
    }

    public boolean validateToken(String param) {
        String key = getRedisKeyFromToken(param);

        if (key != null) {
            User user = userRedis.get(key);
            return user !=null && new DateTime(getExpirationFromToken(param)).isBeforeNow();
        }

        return false;
    }

    public String getRedisKeyFromToken(String token) {
        String key;

        try {
            key = getRealNameFromToken(token);
        } catch (Exception e) {
            key = null;
        }

        return key;
    }

    public String getRealNameFromToken(String token) {
        String realName;

        try {
            final Claims claims = getClaimsFromToken(token);
            realName = claims.getSubject();
        } catch (Exception e) {
            realName = null;
        }

        return realName;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;

        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }

        return claims;
    }

    public Date getExpirationFromToken(String token) {
        Date expiration;

        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = new Date(Long.valueOf(claims.get(CLAIM_EXPIRATION).toString()));
        } catch (Exception e) {
            expiration = null;
        }

        return expiration;
    }
}
