package id.traveney.project.reservation.exception;

import id.traveney.project.reservation.common.StatusCode;

/**
 * Contributor yogaagung
 * Date 26/06/18
 */

public class DefaultException extends RuntimeException {

    public DefaultException(String message) {
        super(message);
    }

    public DefaultException(StatusCode message) {
        super(message.getValue());
    }
}

