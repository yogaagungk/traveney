package id.traveney.project.reservation.config;

import id.traveney.project.reservation.authentication.CustomAuthenticationProvider;
import id.traveney.project.reservation.authentication.JwtAuthenticationFilter;
import id.traveney.project.reservation.authentication.JwtEntryPoint;
import id.traveney.project.reservation.authentication.JwtTokenService;
import id.traveney.project.reservation.redis.UserRedis;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(new JwtEntryPoint()).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/**").permitAll();

        http.addFilterAfter(new JwtAuthenticationFilter(jwtTokenService(), userRedis()),
                UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();
    }

    public CustomAuthenticationProvider customAuthenticationProvider(){
        return new CustomAuthenticationProvider();
    }
    @Bean
    public UserRedis userRedis(){
        return new UserRedis();
    }

    @Bean
    public JwtTokenService jwtTokenService(){
        return new JwtTokenService();
    }
}
